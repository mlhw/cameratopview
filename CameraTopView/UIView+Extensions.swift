//
//  UIView+Extensions.swift
//  CameraTopView
//
//  Created by Mattias Lundhaw on 2017-11-29.
//  Copyright © 2017 ICA Banken. All rights reserved.
//

import UIKit

extension UIView {
    var heightConstraint:NSLayoutConstraint? {
        let constraint = self.constraint(.height)
        return constraint
    }
    
    func constraint(_ attribute:NSLayoutAttribute) -> NSLayoutConstraint? {
        var constraint = (self.constraints.filter{$0.firstAttribute == attribute}.first)
        if (constraint == nil) {
            constraint = self.superview!.constraints.filter{
                guard (self.isEqual($0.firstItem) || self.isEqual($0.secondItem)) else {
                    return false
                }
                return $0.firstAttribute == attribute || $0.secondAttribute == attribute
                
                }.first
        }
        return constraint
    }
}

