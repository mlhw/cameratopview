//
//  ViewController.swift
//  CameraTopView
//
//  Created by Mattias Lundhaw on 2017-11-28.
//  Copyright © 2017 ICA Banken. All rights reserved.
//

import UIKit

class HitTestUITableView: UITableView {
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        
        let hitView = super.hitTest(point, with: event)
        
        if hitView == self {
            return nil
        } else {
            return hitView
        }
    }
}

extension Array where Element:UIView {
    var alpha:CGFloat {
        set {
            for view in self {
                view.alpha = newValue
            }
        }
        get {
            return self.first?.alpha ?? 0
        }
    }
}
class ViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var topView:UIView!
    @IBOutlet weak var actionBackgroundView: UIView!
    @IBOutlet weak var fakeNavbar: UIView!
    @IBOutlet weak var actionButton: UIButton!
    
    let headerContentView:UIView = UIView()
    var headerContentConstraint:NSLayoutConstraint!
    var topHeight:CGFloat {
        let h1 = self.tableView.frame.origin.y
        return h1
    }
    var barHeight:CGFloat = 60
    
    var alphaViews:[UIView] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        setupHeaderContentView()
       setupNavbar()
        
        tableView.delegate = self
        tableView.dataSource = self
        //
        
        
        if #available(iOS 11.0, *) {
            //tableView.contentInsetAdjustmentBehavior = .never
        } else {
            // Fallback on earlier versions
            //automaticallyAdjustsScrollViewInsets = false
        }
        
//        tableView.layer.borderColor = UIColor.red.cgColor
//        tableView.layer.borderWidth = 2
//
//        topView.layer.borderColor = UIColor.green.cgColor
//        topView.layer.borderWidth = 2
        
        actionButton.layer.borderColor = UIColor.white.cgColor
        actionButton.layer.borderWidth = 1
        
        alphaViews = [fakeNavbar, actionBackgroundView]
        
        updateHeight()
        showBar(nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Methods
    
    func setupHeaderContentView() {
        tableView.backgroundView = nil
        tableView.backgroundColor = UIColor.clear
        tableView.isOpaque = false
    }

    
    func setupNavbar() {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
    }
    
    func updateHeight() {
        guard
            let topViewHeight = self.topView.heightConstraint,
            let topViewTop = self.tableView.constraint(NSLayoutAttribute.top)
            else {
                return
        }
    
        var inset = self.tableView.contentInset
        inset.top = topViewHeight.constant-topViewTop.constant
        self.tableView.contentInset = inset
        
        
    }
    
    
    // MARK: - Actions
    
    @IBAction func didPressButton(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        print("Pressed button: \(sender)")
        if sender.isSelected {
            showCamera(sender)
        } else {
            showBar(sender)
        }
        
    }
    
    @IBAction func didPressShowTopView(_ sender: Any?) {
        showCamera(sender)
    }
    
    @IBAction func showCamera(_ sender: Any?) {
        let point = CGPoint(x: 0, y: -(self.tableView.contentInset.top))
        self.tableView.setContentOffset(point, animated: true)
    }
    
    @IBAction func showBar(_ sender: Any?) {
        let point = CGPoint(x: 0, y: -(barHeight))
        self.tableView.setContentOffset(point, animated: true)
    }
    
    @IBAction func hideBar(_ sender: Any?) {
        let point = CGPoint(x: 0, y: 0)
        self.tableView.setContentOffset(point, animated: true)
    }
    
    fileprivate var lastContentOffset: CGFloat = 0
    fileprivate var direction: UISwipeGestureRecognizerDirection?
}

// MARK: TableView Delegate
extension ViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")
        cell?.textLabel?.text = "Row \(indexPath.row)"
        return cell!
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 24
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.01
    }
    
    // MARK: - Scrolling methods
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let per:CGFloat = 100 //percentage of required view to move on while moving collection view
        let deductValue = CGFloat(per / 100 * headerContentView.frame.size.height)
        let offset = (-(per/100)) * (scrollView.contentOffset.y)
        var value = offset - deductValue
        

        let topHeight:CGFloat = self.topHeight
        value+=topHeight
        let minHeight:CGFloat = topHeight
        if value < minHeight {
            value = minHeight
        }
        topView.heightConstraint!.constant = value
        self.view.layoutIfNeeded()
        
        let maxValue = -tableView.contentInset.top
        let minValue = -(barHeight)
        let offsetY = scrollView.contentOffset.y
        let minOffsetY = offsetY-minValue
        
        let percent = 1-(minOffsetY/(maxValue-minValue))
        alphaViews.alpha = percent
        
    }
    

    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {

        // Check what direction the use scrolled
        if scrollView.panGestureRecognizer.translation(in: scrollView).y < 0 {
            print("up")
            self.direction = .up
        } else {
            print("down")
            self.direction = .down
        }
    }
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        guard let direction = self.direction else {
            return
        }
        
        let value = scrollView.contentOffset.y
        let valueMax = self.tableView.contentInset.top
        if (value < -valueMax) {
            // Do nothing
        }
        else if direction == .up {
            if (value < -barHeight) {
                showBar(nil)
            } else if (value > -barHeight) {
                hideBar(nil)
            }
        } else if direction == .down {
            if (value < -barHeight) {
                showCamera(nil)
            } else if (value > -barHeight) {
                showBar(nil)
            }
        }
        
        
        
    }
    
}
